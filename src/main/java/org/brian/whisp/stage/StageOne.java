package org.brian.whisp.stage;

import org.apache.commons.math3.util.Pair;
import org.brian.whisp.AsyncUtils;
import org.brian.whisp.StageController;
import org.brian.whisp.Stages;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Item;

public class StageOne extends AStage {

    private Item attached;
    private boolean isOnHopper = false;
    private boolean isInWater = false;

    private final int attemptsToRemove = 200;
    private int attempts = 0;

    private int attemptsForBlock;

    public StageOne(Item attached){

        super(Stages.ONE);
        this.attached = attached;

    }

    @Override
    public void update(){

        if(!checkForValidity(attached)) {
            //Remove from the MAP
            StageController.getInstance().untrack(attached);
            return;
        }

        Pair<Boolean, Boolean> mech = AsyncUtils.checkMech(attached);
        isOnHopper = mech.getFirst();
        isInWater = mech.getSecond();

        if(isInWater && isOnHopper){

            //Stage 1 finished
            StageController.getInstance().moveToNextStage(attached);

        }

        attempts++;
        if(attempts == attemptsToRemove){
            StageController.getInstance().untrack(attached);
        }

        attemptsForBlock++;
        if(attemptsForBlock == 3){

            if(AsyncUtils.getUnder(attached.getLocation()) != Material.AIR || AsyncUtils.getUnder(attached.getLocation()) != Material.HOPPER) StageController.getInstance().untrack(attached);
            attemptsForBlock = 0;

        }

    }
}
