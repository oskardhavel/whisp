package org.brian.whisp;

import java.util.Arrays;

public enum Stages {

    ONE(1),
    TWO(2),
    THREE(3);

    private int id;
    Stages(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Stages getById(int id){
        return Arrays.stream(values()).filter(v -> v.getId() == id).findFirst().orElse(null);
    }

}
